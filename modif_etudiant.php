<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<?php

include "config.php";

    try{
        $pdo = new PDO("mysql:host=$servername;dbname=$dbname;", $user, $password);
        $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
    }catch(PDO_Exception $x){
        echo "Erreur de connexion ".$x->getMessage();
    }

$recupNom = isset($_POST['nom']) && !empty($_POST['nom']) ? ($_POST['nom']) : ""; 
$recupPrenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? ($_POST['prenom']) : ""; 
$recupMail = isset($_POST['mail']) && !empty($_POST['mail']) ? ($_POST['mail']) : ""; 
$recupTel = isset($_POST['tel']) && !empty($_POST['tel']) ? ($_POST['tel']) : ""; 

$id = isset($_GET["id"]) ? $_GET["id"] : "";
if ( isset($_POST['id']) ) $id = $_POST['id'];
$recupSup = isset($_GET["sup"]) ? $_GET["sup"] : "";

if($recupSup == 'ok'){
    try{
        $req = $pdo->prepare("DELETE FROM etudiants WHERE id_etudiant = :id;");
        $req->execute(['id'=>$id]);
        header("Location: liste_etudiants.php");
    }catch(PDO_Exception $x){
        echo "Erreur supprimer ".$x->getMessage();
    }
}

try{
  $req = $pdo->prepare("SELECT * FROM etudiants WHERE id_etudiant = :id");
  $req->execute(['id'=>$id]);
  //$result = $req->fetchALL();
  $infos = $req->fetch();
}catch(PDO_Exception $x){
  echo "Erreur recup id ".$x->getMessage();
}

// requete pour modifier des infos d'étudiants
if(isset($_POST['submit1'])){
  if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['mail']) && isset($_POST['tel'])){
    try{
        $req = $pdo->prepare("UPDATE etudiants SET nom= :nom, prenom= :prenom, mail= :mail, tel= :tel WHERE id_etudiant= :id");
        $req->execute(['nom'=>$recupNom,
                       'prenom'=>$recupPrenom,
                       'mail'=>$recupMail,
                       'tel'=>$recupTel,
                       'id'=>$id]);
        header("Location: liste_etudiants.php");
      }catch(PDO_Exception $x){
          echo "Erreur modifier ".$x->getMessage();
      }
  }
}




?>


<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index.php" class="brand-link">
        <img src="img/enssop_small.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Administration</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="index.php" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                  Tableau de bord
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="liste_etudiants.php" class="nav-link">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Etudiants
                </p>
              </a>
            </li>
          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Modification des infos d'un étudiant</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Tableau de bord</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                    <div>
                        <form id='form1' action="modif_etudiant.php" method="POST">
                            <input type="hidden" name="id" value="<?php echo $infos["id_etudiant"]?>">
                            <input class='input' type="text" name="nom" value="<?php echo $infos["nom"]?>">
                            <input class='input' type="text" name="prenom" value="<?php echo $infos["prenom"]?>">
                            <input class='input' type="text" name="mail" value="<?php echo $infos["mail"]?>">
                            <input class='input' type="text" name="tel" value="<?php echo $infos["tel"]?>">
                            <center><input class='bt' type="submit" name="submit1" value="Modifier"></center>
                        </form>
                        <center><a href='?sup=ok&id=<?php echo $infos['id_etudiant']; ?>'><input class='bt' type='submit' name='submit2' value='Supprimer'></a></center>
                        
                    </div>
                </div>
                <div class="icon">
                  <i class="fas fa-user-plus"></i>
                </div>
              </div>
            </div>
          </div>
          <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2019-2020 <a href="http://enssop.fr">enssop.fr</a>.</strong>
      All rights reserved.
      <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.0.5
      </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <!-- Bootstrap 4 -->
  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="plugins/moment/moment.min.js"></script>
  <script src="plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
</body>

</html>